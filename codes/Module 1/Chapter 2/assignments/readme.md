#  String Experiment In programming
## Table of contents
- [String Experiment In programming](#string-experiment-in-programming)
  - [Table of contents](#table-of-contents)
  - [AIM](#aim)
  - [Theory](#theory)
  - [Procedure](#procedure)
  - [Simulation](#simulation)
##  AIM

> A string is a sequence of characters -- it can contain a word, your name, a sentence, a paragraph or even an entire composition. A question that arises is that how the contents of a string can be stored. An array of characters seems like a very good choice. Apart from allowing you to store strings, C programming language provides you a large collection of associated string maniputaion functions under the string.h library. These functions will allow you to treat strings like variables and allow operations like addition(concatenation) of two strings, searching a string in another string, compare two strings etc.

## Theory
> In C programming, a string is essentially an array of ASCII code characters. Each character is stored by writing the corresponding 8-bit ASCII code of the character. So, One can initialize a string just like an array:

  `char str[50]={'C',' ','P','r','o','g','r','a','m'};`

  > C programming allows a convenient alternative for this:

  `char str[50]="C programming";`

  >As you can notice, you can put spaces in a string. The C compiler terminates every string with a null character('\0') which has an ASCII value of 0. This is done to mark the end of the string. So, the given string would be represented as:


| C  	|    	| P  	| r   	| o   	| g   	| r   	| a  	| m   	|
|----	|----	|----	|-----	|-----	|-----	|-----	|----	|-----	|
| 67 	| 32 	| 80 	| 114 	| 111 	| 103 	| 114 	| 97 	| 109 	|

>Every string, thus, requires 1 extra storage space to store the NULL character. So, if you want to store a string of 20 characters then you must define an array of 21 characters. To input a string from the keyboard, you can use the scanf statement:
 
 `scanf("%s",str);`

> This will input a string till a space or next line character is entered. Suppose you want to input a string like "Today is a sunny day.". Then you should use the gets() function which is defined in the string.h library.

`gets(str);`

>The string.h library also has many other useful functions. The strlen() is used to find the length of a string(str in this case) and return an integer.

  `int result=strlen(str);`

 >The variable result will get a value 13 in this case. The strcmp() function can be used to compare 2 strings(str1 and str2 in this case):

` int result=strcmp(str1,str2);`
          
The strcmp() function returns 1 if the two strings are identical and 0 otherwise. The strcat() function is used to concatenate 2 string. Writing

`strcmp(str1,str2);`

          
>will add the contents of str2 to str1. Before writing this sentence make sure the there is enough memory allocated at str1 so as to hold both str1 and str2, and also a NULL character. Other useful string functions are listed 
[Here](http://https://en.wikipedia.org/wiki/C_string_handling "wikipedia").

## Procedure
![home](home.png)
* String Matching
   1. Press start to start the experiment and select two string str1 and str2 to compare.
   2. Press next to see the execution of the code.
   3. Relavant line in the code is shown here
   4. The output of the code is shown in the right
* String Comparison
   1. Press start to start the experiment and select two string str1 and str2 to compare.
   2. Press next to see the execution of the code
   3. The output of the code is shown in the right
   4. u can stop the code using stop button

## Simulation

* ## String matching
    1. write string in the boxes ![step1](c1.png)
    2. Hit on the ok then start button and  it simmulate each and every step of the code with red highlight ![step2](c2.png)
    3. Both the string are  same  ![step3](c3.png)
    4. string 2 is greater than string 1  ![step4](c4.png)
* ## String Comparison
    1. write string in the boxes ![step1](m1.png)
    2. Hit on the ok then start button and  it simmulate each and every step of the code with red highlight ![step2](m2.png)
    3. Both the string are  same  ![step3](m3.png)
    4. string 2 is greater than string 1  ![step4](m4.png)